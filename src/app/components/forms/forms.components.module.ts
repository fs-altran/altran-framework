import { NgModule } from '@angular/core';
import { FormSelectComponent } from './form-select/form-select.component';
import { FormInputComponent } from './form-input/form-input.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormBaseComponent } from './form-base/form-base.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        FormSelectComponent,
        FormInputComponent,
        FormBaseComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        FormSelectComponent,
        FormInputComponent
    ]
})
export class AltranFormComponentsModule { }
