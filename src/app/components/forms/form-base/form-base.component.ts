import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'form',
  templateUrl: './form-base.component.html',
  styleUrls: ['./form-base.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class FormBaseComponent implements OnInit, ControlValueAccessor {

  // Basic Common @Inputs
  @Input() formTitle: string;
  @Input() styleType: string;
  @Input() disabled: boolean;
  @Input() error: string;


  @Output() isFocused: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() dataChanged: EventEmitter<any> = new EventEmitter<any>();



  // UI Vars
  protected isFloating = false;


  // Data Vars
  protected innerValue: any;
  protected onTouchedCallback: () => void = () => { };
  protected onChangeCallback: (_: any) => void = () => { };




  // *******************************************
  // LIFECYCLE Methods
  // *******************************************
  constructor() { }

  ngOnInit() { }






  // *******************************************
  // UI Methods
  // *******************************************
  protected focus(isFocus: boolean) {
    this.isFocused.emit(isFocus);
  }






  // *******************************************
  // DATA Methods
  // *******************************************
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
      this.dataChanged.emit(v);
    }
  }


  get value() {
    return this.innerValue;
  }






  // *******************************************
  // IMPLEMENTS ControlValueAccessor Methods
  // *******************************************
  public writeValue(value: any) {
    if (value !== this.innerValue) {
      setTimeout(() => {
        this.innerValue = value;
      }, 2);
    }
  }


  public registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }


  public registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
