import { Component, OnInit, ViewEncapsulation, Renderer2 } from '@angular/core';
import { FormBaseComponent } from '../form-base/form-base.component';

@Component({
  selector: 'fom-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class FormSelectComponent extends FormBaseComponent implements OnInit {

  constructor(private renderer: Renderer2) {
    super();
  }

  ngOnInit() {
    this.renderer.selectRootElement('fom-select', true);
   }

}
