import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormBaseComponent } from '../form-base/form-base.component';

@Component({
  selector: 'control-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class FormInputComponent implements OnInit {

  @Input() test: boolean;

  constructor() { }

  ngOnInit() { }

}
